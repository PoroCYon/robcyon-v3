module RobCYon.Module(handleEvent) where

import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Monad.Reader
import Data.Maybe
import Data.Some
import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types

import RobCYon.Command
import RobCYon.Types

handleEvent :: UnicodeEvent -> StatefulIRC BotState ()
handleEvent e = do
    tstate <- asks _userState
    state  <- liftIO $ readTVarIO tstate

    modules' <- mapM (invokeModule e) $ modules state

    let state' = state { modules = modules' }

    liftIO $ atomically $ writeTVar tstate state'
    where
        invokeModule e (This mod) =
            let s = mstate mod in
            case mhandler mod of
                IrcEvent   eh -> do
                    nso <- eh s e
                    case nso of
                        Just ns -> return $ This $ mod { mstate = ns }
                        Nothing -> return $ This   mod
                IrcCommand cs -> do
                    nso <- execCommands cs s e
                    case nso of
                        Just ns -> return $ This $ mod { mstate = ns }
                        Nothing -> return $ This   mod

