module RobCYon.Types (
          BotServer  (..)
        , BotEvent   (..)
        , IgnoreUser (..)
        , IdentInfo  (..)
        , IrcEventHandler
        , BotCallback(..)
        , BotCommand (..)
        , BotEventHandler(..)
        , BotModule  (..)
        , BotState   (..)
        ) where

import Data.Some
import Data.Text (Text)
import Data.Time.Clock (NominalDiffTime)

import Network.IRC.Client.Types

data BotServer = BotServer { hostname :: String         ,
                             port     :: Int            ,
                             floodCD  :: NominalDiffTime,
                             ssl      :: Bool           }
                    deriving (Show, Eq)

data BotEvent s = BotEvent { descr   :: Text,
                             etype   :: EventType,
                             handler :: UnicodeEvent -> StatefulIRC s () }
instance Show (BotEvent s) where
    show e   = "Event handler for: " ++ show (etype e) ++ ", \"" ++ show (descr e) ++ "\""
instance Eq   (BotEvent s) where
    (==) a b = descr a == descr b && etype a == etype b

data IgnoreUser = IgnoreUser { ignuser :: Text       ,
                               channel :: Maybe Text }
                    deriving (Show, Eq)

data IdentInfo = IdentInfo { nickname :: Text         ,
                             username :: Maybe Text   ,
                             realname :: Maybe Text   ,
                             password :: Maybe Text   ,
                             channels :: [Text]       ,
                             ctcpVer  :: Text         ,
                             ignores  :: [IgnoreUser] }
                    deriving (Show, Eq)

type IrcEventHandler s = s -> UnicodeEvent -> StatefulIRC BotState (Maybe s)

data BotCallback s = SimpleCb   (     UnicodeEvent -> Text -> Text                          )
                   | StatefulCb (s -> UnicodeEvent -> Text -> StatefulIRC BotState (Maybe s))

data BotCommand s = BotCommand { callback  :: BotCallback s,
                                 aliases   :: [Text]       ,
                                 isControl :: Bool         }
instance Show (BotCommand s) where
    show cmd = "Aliases: " ++ show (aliases cmd) ++ ", requires control: " ++ show (isControl cmd)
instance Eq   (BotCommand s) where
    (==) a b = aliases a == aliases b && isControl a == isControl b

data BotEventHandler s = IrcEvent   (IrcEventHandler s)
                       | IrcCommand [BotCommand      s]

data BotModule s = BotModule { mname    :: String,
                               mstate   :: s     ,
                               mhandler :: BotEventHandler s }

data BotState = BotState { controllers :: [Text          ] ,
                           modules     :: [Some BotModule] }

