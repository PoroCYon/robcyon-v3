module RobCYon.IRC (run) where

import Data.ByteString (ByteString)
import Data.List       (union     )
import Data.Maybe      (fromMaybe )
import Data.Text       (Text      )

import qualified Data.ByteString       as BS
import qualified Data.ByteString.Char8 as C8
import qualified Data.Text as T

import Network.IRC.Client
import Network.IRC.Client.Types

import RobCYon.Types

getIgnore :: IgnoreUser -> (Text, Maybe Text)
getIgnore ign = (ignuser ign, channel ign)

getHandler :: BotEvent s -> EventHandler s
getHandler event =
    EventHandler { _description = descr   event,
                   _matchType   = etype   event,
                   _eventFunc   = handler event }

getConfig :: IdentInfo -> [BotEvent s] -> InstanceConfig s
getConfig ident handlers =
    let n = nickname ident in
    InstanceConfig { _nick     = n             ,
                     _username = fromMaybe n $ username ident,
                     _realname = fromMaybe n $ realname ident,
                     _password = password ident,
                     _channels = channels ident,
                     _ctcpVer  = ctcpVer  ident,

                     _ignore        = map getIgnore $ ignores ident,
                     _eventHandlers = map getHandler  handlers     }

mergeConfig :: InstanceConfig s -> InstanceConfig s -> InstanceConfig s
mergeConfig a b =
    InstanceConfig { _nick     = getstr _nick     a b           ,
                     _username = getstr _username a b           ,
                     _realname = getstr _realname a b           ,
                     _password = getpw (_password a) (_password b),
                     _channels = _channels a `union` _channels b,
                     _ctcpVer  = getstr _ctcpVer  a b           ,
                     _ignore   = _ignore a ++ _ignore b         ,
                     _eventHandlers = _eventHandlers a ++ _eventHandlers b }
    where
        getstr get a b = let gb = get b in if T.null $ gb then get a else gb

        getpw a Nothing = a
        getpw _ b       = b

run :: BotServer -> IdentInfo -> [BotEvent s] -> s -> IO ()
run serv ident events state = do
    -- TODO: make logger optional/modifiable
    let connect' = if ssl serv then connectWithTLS' else connect'
    connection <- connect' stdoutLogger (C8.pack $ hostname serv) (port serv) (floodCD serv)
    let config = mergeConfig (defaultIRCConf $ nickname ident) $ getConfig ident events

    startStateful connection config state

