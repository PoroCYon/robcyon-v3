module RobCYon.Modules.Brainfuck (m0dule) where

import Control.Monad.Reader
import Data.Text (Text)
import Language.Brainfuck
import Network.IRC.Client
import Network.IRC.Client.Types
import Network.IRC.Client.Utils

import qualified Data.Text as T

import RobCYon.Types

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Brainfuck"          ,
                     mstate   = ()                   ,
                     mhandler = IrcCommand [ bfCmd ] }
    where
        bfCallback _ e t = do
            let s = makeState (T.unpack t) [] 0x2000 -- should be enough -- TODO: parse parameters
            let r = evaluate s
            reply e $ T.pack r
            return Nothing
        bfCmd = BotCommand { callback  = StatefulCb bfCallback            ,
                             isControl = False                            ,
                             aliases   = map T.pack [ "brainfuck", "bf" ] }
