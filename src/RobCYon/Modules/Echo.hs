module RobCYon.Modules.Echo (m0dule) where

import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types

import qualified Data.Text as T

import RobCYon.Types

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Echo"                 ,
                     mstate   = ()                     ,
                     mhandler = IrcCommand [ echoCmd ] }
    where
        echoCallback _ = id
        echoCmd = BotCommand { callback  = SimpleCb echoCallback,
                               isControl = False,
                               aliases   = map T.pack ["echo", "say", "id"] }
