module RobCYon.Modules.Quit (m0dule) where

import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types
import Network.IRC.Client.Utils

import qualified Data.Text as T

import RobCYon.Types

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Quit"                 ,
                     mstate   = ()                     ,
                     mhandler = IrcCommand [ quitCmd ] }
    where
        quitCallback _ e t = do
            disconnect
            return Nothing
        quitCmd = BotCommand { callback  = StatefulCb quitCallback                    ,
                               isControl = True                                       ,
                               aliases   = map T.pack ["quit", "kill", "exit", "die"] }
