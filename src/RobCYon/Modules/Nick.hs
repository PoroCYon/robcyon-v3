module RobCYon.Modules.Nick (m0dule) where

import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types
import Network.IRC.Client.Utils

import qualified Data.Text as T

import RobCYon.Types

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Nick"                 ,
                     mstate   = ()                     ,
                     mhandler = IrcCommand [ nickCmd ] }
    where
        nickCallback _ e t =
            if not $ T.null t
            then do
                setNick t
                return Nothing
            else do
                reply e $ T.pack "Not enough args: expected new nickname."
                return Nothing
        nickCmd = BotCommand { callback  = StatefulCb nickCallback,
                               isControl = True                   ,
                               aliases   = map T.pack [ "nick" ]  }
