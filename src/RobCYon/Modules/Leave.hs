module RobCYon.Modules.Leave (m0dule) where

import Control.Monad.Reader
import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types
import Network.IRC.Client.Utils

import qualified Data.Text as T

import RobCYon.Types

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Leave"                 ,
                     mstate   = ()                      ,
                     mhandler = IrcCommand [ leaveCmd ] }
    where
        leaveCallback _ e t =
            let args = T.words t in
            case _source e of
                (Channel chan _) -> do
                    liftIO $ putStrLn $ "leaving " ++ T.unpack t
                    leaveChannel (if null args then chan else head args)
                        -- skip the first arg: channel name to leave
                        (if null args || null (tail args) then Nothing else Just $ T.unwords $ tail args)
                    return Nothing
                _                -> return Nothing
        leaveCmd = BotCommand { callback  = StatefulCb leaveCallback,
                                isControl = True                    ,
                                aliases   = map T.pack [ "leave" ]  }
