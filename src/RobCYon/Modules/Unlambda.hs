module RobCYon.Modules.Unlambda (m0dule) where

import Control.Monad.Reader
import Data.Char (toLower)
import Data.List (null, lines, unlines, concat)
import Data.Text (Text)
import Language.Unlambda
import Network.IRC.Client
import Network.IRC.Client.Types
import Network.IRC.Client.Utils

import RobCYon.Types

import qualified Data.Text as T

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Unlambda"            ,
                     mstate   = ()                    ,
                     mhandler = IrcCommand [ unlCmd ] }
    where
        unlCallback _ e t =
            case parse $ T.unpack t of
                Just (_, ex) -> do
                    let (Eval cp) = eval ex
                    r <- liftIO $ cp (Nothing, 0x2000) (const return)
                    reply e $ T.pack $ show r
                    return Nothing
                Nothing -> return Nothing
        unlCmd = BotCommand { callback  = StatefulCb unlCallback           ,
                              isControl = False                            ,
                              aliases   = map T.pack [ "unlambda", "unl" ] }
