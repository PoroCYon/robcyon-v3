module RobCYon.Modules.Join (m0dule) where

import Control.Monad.Reader
import Data.List (elem)
import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types
import Network.IRC.Client.Utils

import qualified Data.Text as T

import RobCYon.Types

m0dule :: BotModule ()
m0dule = BotModule { mname    = "Join"                 ,
                     mstate   = ()                     ,
                     mhandler = IrcCommand [ joinCmd ] }
    where
        joinCallback _ e t =
            if not $ T.null t
            then do
                ic <- instanceConfig
                if elem t (_channels ic) then do
                    reply e $ T.pack "I am already in that channel."
                    return Nothing
                else
                    let ic' = ic { _channels = t : _channels ic }
                    in do putInstanceConfig ic' -- update internal channel list
                          send $ Join t -- ... there's no helper method for this?
                          return Nothing
            else do
                reply e $ T.pack "Not enough args: I need a channel name to join."
                return Nothing
        joinCmd = BotCommand { callback  = StatefulCb joinCallback,
                               isControl = True                   ,
                               aliases   = map T.pack [ "join" ]  }
