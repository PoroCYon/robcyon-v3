module RobCYon.Utils (
          commandPrefixes
        , getNameN
        , getNameC
        , getText
        , isController
        ) where

import Data.List (elem)
import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types

commandPrefixes :: [Char]
commandPrefixes =  [':' ]

getNameN :: Source a -> a
getNameN (User         nick) = nick
getNameN (Channel chan nick) = nick
getNameN (Server  serv     ) = serv

getNameC :: Source a -> a
getNameC (User         nick) = nick
getNameC (Channel chan nick) = chan
getNameC (Server  serv     ) = serv

getText :: Message a -> Maybe a
getText (Privmsg _ (Right t)) = Just t
getText (Notice  _ (Right t)) = Just t
getText                   _   = Nothing

isController :: [Text] -> Source Text -> Bool
isController ctls (User         nick) = elem nick ctls
isController ctls (Channel chan nick) = elem nick ctls -- todo: give chan ops certain powers?
isController ctls  _                  = False
