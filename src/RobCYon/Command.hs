module RobCYon.Command (execCommands) where

import Control.Concurrent.STM
import Control.Concurrent.STM.TVar
import Control.Monad.Reader (asks, liftIO)
import Data.List (elem, notElem, foldr)
import Data.Maybe (fromMaybe)
import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types

import qualified Data.Text as T

import RobCYon.Types
import RobCYon.Utils

doCommand :: BotCommand s -> s -> UnicodeEvent -> Text -> StatefulIRC BotState (Maybe s)
doCommand cmd s e ctext = do
    tstate <- asks _userState
    state  <- liftIO $ readTVarIO tstate

    if isControl cmd <= (isController (controllers state) $ _source e)
    then case callback cmd of
              SimpleCb   cb ->
                  let r = cb e ctext in
                  if not $ T.null r then do
                      reply e r
                      return Nothing
                  else return Nothing
              StatefulCb cb -> cb s e ctext
    else case _source e of
              (Server _) -> return Nothing -- don't send the message to a server
              (       _) -> do
                    reply e $ imsorry `T.append` (getNameN $ _source e) `T.append` cantlet
                    return Nothing
    where
        imsorry = T.pack "I'm sorry, "
        cantlet = T.pack ", but I'm afraid I can't let you do that."

execCommands :: [BotCommand s] -> s -> UnicodeEvent -> StatefulIRC BotState (Maybe s)
execCommands cmds s e =
    let msg' = getText $ _message e in
    case msg' of
        Just msg ->
            if T.null msg || notElem (T.head msg) commandPrefixes then return Nothing
            else
                let matchesMsg = matches msg in
                let matching = filter (\ c -> any matchesMsg $ aliases c) cmds
                in if null matching then return Nothing
                   else
                       let cmd = head matching in
                       let alias = head $ filter matchesMsg $ aliases cmd
                       in doCommand cmd s e (T.drop (T.length alias + 2 {- cmd prefix + trailing space -}) msg)
                                                                        -- it's safe if alias + 2 is longer than msg,
                                                                        -- according to the spec (and implementation)
        Nothing -> return Nothing
    where
        space = T.pack " "
        matches msg a' =
            let m = T.toUpper $ T.tail msg in
            let a = T.toUpper a'
            in a == m || (a `T.append` space) `T.isPrefixOf` m
