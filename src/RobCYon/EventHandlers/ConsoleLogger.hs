module RobCYon.EventHandlers.ConsoleLogger (eventHandler) where

import Control.Monad.Reader
import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types

import qualified Data.Text as T

import RobCYon.Types

eventHandler :: BotEvent BotState
eventHandler =  BotEvent { descr   = description,
                           etype   = EEverything,
                           handler = logConsole }
    where
        description  = T.pack "Console-logging event handler"
        logConsole e = do liftIO $ putStrLn $ show $ _message e
