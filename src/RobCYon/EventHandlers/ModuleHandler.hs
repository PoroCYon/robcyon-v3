module RobCYon.EventHandlers.ModuleHandler (eventHandler) where

import Data.Text (Text)
import Network.IRC.Client
import Network.IRC.Client.Types

import qualified Data.Text as T

import RobCYon.Module
import RobCYon.Types

eventHandler :: BotEvent BotState
eventHandler = BotEvent { descr   = description ,
                          etype   = EEverything ,
                          handler = handleEvent }
    where
        description = T.pack "Module-handling event handler"
