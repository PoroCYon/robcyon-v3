module Main where

import Data.Some
import Network.IRC.Client
import Network.IRC.Client.Types

import RobCYon.IRC
import RobCYon.Types

import qualified RobCYon.EventHandlers.ConsoleLogger as ConsoleLogger
import qualified RobCYon.EventHandlers.ModuleHandler as ModuleHandler

import qualified RobCYon.Modules.Echo  as Echo
import qualified RobCYon.Modules.Quit  as Quit
import qualified RobCYon.Modules.Leave as Leave
import qualified RobCYon.Modules.Nick  as Nick
import qualified RobCYon.Modules.Join  as Join
import qualified RobCYon.Modules.Brainfuck as BF
import qualified RobCYon.Modules.Unlambda  as UNL

import Params (paramCtls, paramServer, paramIdent) -- deliberatly included in .gitignore

main :: IO ()
main = run paramServer paramIdent events initState
    where
        initState = BotState { controllers = paramCtls          ,
                               modules     = This UNL.m0dule : This BF.m0dule : map This basicMods }
        basicMods = [ Echo.m0dule, Quit.m0dule, Leave.m0dule, Nick.m0dule, Join.m0dule ]
        events    = [ {-ConsoleLogger.eventHandler,-} ModuleHandler.eventHandler ]

